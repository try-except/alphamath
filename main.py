import sys  # Allows terminal arguments to be passed
from PyQt6.QtWidgets import QApplication
from frontend.windows import (
    GameMenuWindow,
    HelpWindow,
    QuestionWindow,
    screen_resizer,
    StartMenuWindow,
)
from backend.game import Game

def hook(type_error, traceback):
    """
    If no stderr is being printed, replace default excepthook with this one
    """
    print(traceback)
    print(traceback)

#sys.__excepthook = hook
app = QApplication(sys.argv)  # This is the main Application object
app.setApplicationName("Alphamath")
screen_resizer(
    (app.primaryScreen().size().width(), app.primaryScreen().size().height())
)

# Frontend Windows
start_menu_window = StartMenuWindow()
question_window = QuestionWindow()
game_menu_window = GameMenuWindow()
help_window = HelpWindow()

# Backend Objects
game = Game()

# Signal connections
## Start Menu
start_menu_window.start_game_signal.connect(game.pick_question)
## Game Backend
game.question_signal.connect(question_window.recieve_question)
## Question Window
question_window.next_screen_signal.connect(game_menu_window.show)
## Game Menu Window
#game_menu_window.next_screen_signal.connect(answer_window.show)
game_menu_window.previous_screen_signal.connect(question_window.show)
game_menu_window.help_signal.connect(help_window.show)
## Help Window
help_window.previous_screen_signal.connect(game_menu_window.show)


sys.exit(app.exec())