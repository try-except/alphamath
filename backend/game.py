from PyQt6.QtCore import QObject, pyqtSignal

class Game(QObject):
    """
    This is the game's main backend object
    """
    question_signal = pyqtSignal(str)
    next_screen_signal = pyqtSignal()

    def __init__(self):
        super().__init__()
    
    def pick_question(self):
        """
        Randomly selects a question from the database and sends it to the
        frontend
        """
        self.question_signal.emit("Hello World!")