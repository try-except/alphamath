from PyQt6.QtWidgets import QLabel, QVBoxLayout, QPushButton
from PyQt6.QtCore import Qt, pyqtSignal, QTimer
from PyQt6.QtGui import QPixmap
from frontend.classes import RaspberryWindow

def screen_resizer(new_size: tuple) -> None:
    """
    A simple wrapper function that modifies RaspberryWindow's class var
    """
    RaspberryWindow.screen_res = new_size

class StartMenuWindow(RaspberryWindow):
    start_game_signal = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.init_gui()
    
    def init_gui(self):
        """
        Initializes GUI elements and calls the show method
        """
        self.main_layout = QVBoxLayout(self)

        ## Banner
        banner = QLabel(self)
        banner.setScaledContents(True)
        banner_pixmap = QPixmap("frontend/assets/Dino.png")
        banner.setPixmap(banner_pixmap)

        ## Buttons
        self.start_button = QPushButton("Comenzar", self)
        self.exit_button = QPushButton("Salir", self)

        # Signal Connection
        self.start_button.clicked.connect(self.start_game)
        self.exit_button.clicked.connect(self.close)

        # Add widgets to layout
        self.main_layout.addWidget(banner)
        self.main_layout.addWidget(self.start_button)
        self.main_layout.addWidget(self.exit_button)

        self.show()

    def start_game(self):
        self.start_game_signal.emit()
        self.close()

class QuestionWindow(RaspberryWindow):

    def __init__(self):
        super().__init__()
        self.enable_next_button_timer = QTimer(self)
        self.enable_next_button_timer.setSingleShot(True)
        self.enable_next_button_timer.timeout.connect(self.enable_next_button)
        self.init_gui()
    
    def init_gui(self):
        self.main_layout = QVBoxLayout(self)

        ## Labels
        self.question_label = QLabel(self)

        ## Banner
        banner = QLabel(self)
        banner.setScaledContents(True)
        banner_pixmap = QPixmap("frontend/assets/Dino.png")
        banner.setPixmap(banner_pixmap)

        ## Buttons
        self.next_button = QPushButton("Continuar", self)
        self.next_button.setEnabled(False)

        # Signal Connection
        self.next_button.clicked.connect(self.next_screen)

        # Add widgets to layout
        self.main_layout.addWidget(self.question_label)
        self.main_layout.addWidget(banner)
        self.main_layout.addWidget(self.next_button)

    def recieve_question(self, question):
        self.question_label.setText(question)
        self.enable_next_button_timer.start(500)
        self.show()
    
    def next_screen(self):
        self.next_screen_signal.emit()
        self.close()
    
    def enable_next_button(self):
        self.next_button.setEnabled(True)

class GameMenuWindow(RaspberryWindow):
    """
    This is the class for the game menu that allows the player to choose
    between asking for help or answering the question
    """

    # Signals
    help_signal = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.init_gui()
    
    def init_gui(self):
        self.main_layout = QVBoxLayout(self)

        ## Buttons
        self.answer_button = QPushButton("Contestar", self)
        self.help_button = QPushButton("Pedir Ayuda", self)
        self.back_button = QPushButton("Atrás", self)

        # Signal Connection
        self.answer_button.clicked.connect(self.answer)
        self.help_button.clicked.connect(self.ask_for_help)
        self.back_button.clicked.connect(self.go_back)

        # Add widgets to layout
        self.main_layout.addWidget(self.answer_button)
        self.main_layout.addWidget(self.help_button)
        self.main_layout.addWidget(self.back_button)

    def answer(self):
        self.next_screen_signal.emit()
        self.close()
    
    def ask_for_help(self):
        self.help_button.setEnabled(False)
        self.help_signal.emit()
        self.close()
    
    def go_back(self):
        self.previous_screen_signal.emit()
        self.close()

class HelpWindow(RaspberryWindow):
    def __init__(self):
        super().__init__()
        self.enable_back_button_timer = QTimer(self)
        self.enable_back_button_timer.setSingleShot(True)
        self.enable_back_button_timer.timeout.connect(
            self.enable_back_button)
        self.init_gui()
    
    def init_gui(self):
        self.main_layout = QVBoxLayout(self)

        ## Buttons
        self.back_button = QPushButton("Continuar", self)

        # Signal Connection
        self.back_button.clicked.connect(self.go_back)

        # Add widgets to layout
        self.main_layout.addWidget(self.back_button)
    
    def show(self):
        self.back_button.setEnabled(False)
        self.enable_back_button_timer.start(500)
        super().show()
    
    def go_back(self):
        self.previous_screen_signal.emit()
        self.close()
    
    def enable_back_button(self):
        self.back_button.setEnabled(True)