"""
This module contains the PyQt subclasses used in the game.
TODO:
    * Add README and/or wiki entry
"""

# library imports
from PyQt6.QtWidgets import QLabel
from PyQt6.QtCore import Qt, pyqtSignal

class RaspberryWindow(QLabel):
    """
    Since all windows should be shown in Fullscreen, this class includes some
    defaults
    """

    screen_res = None  # this value gets initialized on app startup
    next_screen_signal = pyqtSignal()
    previous_screen_signal = pyqtSignal()

    def __init__(self):#, screen_res):
        super().__init__()
        self.fullscreen = True
        self.set_size(self.screen_res)
    
    def set_size(self, screen_res):
        """
        Sets window size and default show method depending on screen resolution
        """
        if screen_res != (480, 320):
            self.setFixedSize(480, 320)
            self.fullscreen = False
    
    def show(self):
        if self.fullscreen:
            return self.showFullScreen()
        super().show()